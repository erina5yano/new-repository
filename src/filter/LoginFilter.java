package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	public static String INIT_PARAMETER_NAME_LOGIN = "login";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

	String path =((HttpServletRequest)request).getServletPath();


	HttpSession session = ((HttpServletRequest)request).getSession();
	User loginUser = (User) session.getAttribute("loginUser");

	if(loginUser==null) {

		if(!(path.equals("/login"))){
			if(!(path.contains("/css")))

			((HttpServletResponse)response).sendRedirect("login");
			return;

		}

    }


//セッションの中にloginUser情報があるか loginUser

//手打ちしているURL


		chain.doFilter(request, response); // サーブレットを実行
	}

	//ログインしているかのチェック
	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

}


