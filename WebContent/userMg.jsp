<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"type="text/css" href="./css/top.css">
<title>ユーザー管理</title>
</head>
<body>

<div class="main-contents">
		<div class="header">
			<a href="signup">新規登録</a>
		</div>
		<c:forEach items="${users}" var="user">
		<div style ="padding: 10px; margin-bottom: 10px; border: 1px solid #333333; border-radius:10px;">
		<div class = "users">
		<c:out value = "${user.id}"/>
		</div>
		ID <span class="id"><c:out value="${user.id}" /></span><br>
		ログインID <span class="account"><c:out value="${user.account}" /></span><br>
		ユーザー名 <span class="name"><c:out value="${user.name}" /></span><br>
		支社 <span class="branch_id"><c:out value="${user.branch_id}" /></span><br>
		部署・役職 <span class="department_id"><c:out value="${user.department_id}" /></span><br>
		状態 <span class="is_stopped"><c:out value="${user.is_stopped}" /></span><br>

		<c:if test="${user.is_stopped == 0}">
			<form action="stop" method="post">
			<input type = "hidden" name= "is_stopped" id="is_stopped" value="${user.is_stopped}"/>
			<input type = "hidden" name= "id" id="id" value="${user.id}"/>

			<input type="submit" value=" 停止させる " onclick="alert('本当に変更しますか？');" /><br>
			</form>
		</c:if>
				<c:if test="${user.is_stopped == 1}">
			<form action="stop" method="post">
			<input type = "hidden" name= "is_stopped" id="is_stopped" value="${user.is_stopped}"/>
			<input type = "hidden" name= "id" id="id" value="${user.id}"/>

			<input type="submit" value=" 復活させる " onclick="alert('本当に変更しますか？');" /><br>
			</form>
		</c:if>

		登録日時 <span class="created_date"><c:out value="${user.created_date}" /></span><br>
		更新情報 <span class="updated_date"><c:out value="${user.updated_date}" /></span><br>

        <form action="edit?id=${user.id}" method="get">
		<input type = "hidden" name= "id" id="id" value="${user.id}"/>
		<input type="submit" value="編集" />
		</div>
		</form>
		</c:forEach>
		<a href="./">戻る</a>
</div>
</body>
</html>