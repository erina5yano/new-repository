package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {//ユーザー新規登録

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password ");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", 0");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String account,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User edit(Connection connection,int id ) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {


        List<User> ret = new ArrayList<User>();

        try {
            while (rs.next()) {

                int id = rs.getInt("id");
                String account= rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int department_id = rs.getInt("department_id");
                int is_stopped= rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setDepartment_id(department_id);

                user.setIs_stopped(is_stopped);
                user.setCreated_date(createdDate);
                user.setUpdated_date(updatedDate);

                ret.add(user);

            }
            return ret;

        } finally {
            close(rs);

        }
    }

    public List<User> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
        	String sql = "SELECT * FROM users";
        	ps = connection.prepareStatement(sql);

        	ResultSet rs = ps.executeQuery();

            List<User> userList = toUserList(rs);



            if (userList.isEmpty() == true) {
                return null;
            }else {
                return userList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void is_stopped(Connection connection, int is_stopped, int id) {

        PreparedStatement ps = null;
        try {


            ps = connection.prepareStatement("UPDATE users SET is_stopped=? WHERE id =?");

         if(is_stopped==1) {
        	 ps.setInt(1, is_stopped=0);//この辺が不安　is_stoppedの部分を停止の状態の1にするのか
         }else{
        	 ps.setInt(1, is_stopped=1);

         }

            ps.setInt(2, id);

            ps.executeUpdate();//データベースに登録するメソッド

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public void update(Connection connection, int id,User updateUser) {//更新

        PreparedStatement ps = null;
        try {


            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("account = ?");

            if (StringUtils.isEmpty(updateUser.getPassword()) == false){
            	sql.append(", password = ? ");
            sql.append(", name = ? ");
            sql.append(", branch_id = ? ");
            sql.append(", department_id = ? ");
            sql.append(" WHERE");
            sql.append(" id = ? ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, updateUser.getAccount());
            if (StringUtils.isEmpty(updateUser.getPassword()) == false){
            ps.setString(2, updateUser.getPassword());
            ps.setString(3, updateUser.getName());
            ps.setInt(4, updateUser.getBranch_id());
            ps.setInt(5, updateUser.getDepartment_id());
            ps.setInt(6, id);
            ps.executeUpdate();
          }else {
              ps.setString(2, updateUser.getName());
              ps.setInt(3, updateUser.getBranch_id());
              ps.setInt(4, updateUser.getDepartment_id());
              ps.setInt(5, id);

          }

            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);

       }
    }

}
