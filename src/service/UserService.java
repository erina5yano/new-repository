package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();


            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<User> getUsers() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userMgDao = new UserDao();


            List<User> ret = userMgDao.getUsers(connection);
            System.out.println(ret);
            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void stop(int is_stopped,int id){ //ここから停止

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.is_stopped(connection, is_stopped,id);


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User edit(int id){ //ここから編集

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
           User user = userDao.edit(connection,id);
           //

            commit(connection);
            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void update(int id,User updateUser){

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(updateUser.getPassword());
            updateUser.setPassword(encPassword);

           UserDao userDao = new UserDao();
           userDao.update(connection, id,updateUser);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}