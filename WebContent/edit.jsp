<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<body>

<div class="main-contents">
		<div class="header">
		</div>
		<c:out value="${users}"/>
	<div class = "users">
		ID <span class="id"><c:out value="${user.id}" /></span><br>
		ログインID <span class="account"><c:out value="${user.account}" /></span><br>

		ユーザー名 <span class="name"><c:out value="${user.name}" /></span><br>
		支店 <span class="branch_id"><c:out value="${user.branch_id}" /></span><br>
		部署・役職 <span class="department_id"><c:out value="${user.department_id}" /></span><br>
		状態 <span class="is_stopped"><c:out value="${user.is_stopped}" /></span><br>
		登録日時 <span class="created_date"><c:out value="${user.created_date}" /></span><br>
		更新情報 <span class="updated_date"><c:out value="${user.updated_date}" /></span><br>
	</div>
	<br>
		<form action="edit" method="post">
            <label for="account">ログインID</label><input name= "account" id= "account"  value="${user.account}"/><br />
            <label for="password">パスワード(変更)</label><input name= "password" id= "password"/><br />
            <label for="password">パスワード(確認)</label><input name= "password" id= "password"/><br />
            <label for="name">ユーザー名</label><input name= "name" id= "name"  value="${user.name}"/><br />
            <label for="branch_id">支店</label><input name= "branch_id" id= "branch_id"  value="${user.branch_id}"/><br />
            <label for="department_id">部署</label><input name= "department_id" id= "department_id"  value="${user.department_id}"/><br />
			<input type = "hidden" name= "id" id="id" value="${user.id}"/>
		<input type="submit" value="変更" />
		<br>
		<a href="./">トップ画面へ</a>
	   </form>



</div>>
</body>
</html>