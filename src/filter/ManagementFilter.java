package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/usermg","/signup", "/edit"})
public class ManagementFilter implements Filter {

	public static String INIT_PARAMETER_NAME_MANAGEMENT = "management";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

	HttpSession session = ((HttpServletRequest)request).getSession();
	User loginUser = (User) session.getAttribute("loginUser");

	    if (loginUser.getBranch_id() != 1 && loginUser.getDepartment_id () != 1){

				((HttpServletResponse)response).sendRedirect("./");
				return;

				
			}
	          chain.doFilter(request, response);
	    }




	@Override
	public void destroy() {

	}

}