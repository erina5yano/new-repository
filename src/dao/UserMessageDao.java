package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;


public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, String start, String end, String search) {//ここに追加するか

        PreparedStatement ps = null;
        try {


        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id,");
            sql.append("users.name as name,");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date ");
	        sql.append("BETWEEN ? ");
	        sql.append("AND ? ");

            //カテゴリが通る時→カテゴリが空じゃないとき
	        if(!StringUtils.isEmpty(search)){
        	   sql.append("AND messages.category LIKE ? ");
	        }
            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, start);
            ps.setString(2, end);

            if(!StringUtils.isEmpty(search)) {
            	ps.setString(3, "%" + search + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {

            while (rs.next()) {

            	int id = rs.getInt("id");
            	String title = rs.getString("title");
            	String text = rs.getString("text");
            	String category = rs.getString("category");
                int user_id = rs.getInt("user_id");
                String name = rs.getString("name");
                Timestamp created_date = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setTitle(title);
                message.setText(text);
                message.setCategory(category);
                message.setUser_id(user_id);
                message.setName(name);
                message.setCreated_date(created_date);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}

//    public void message(Connection connection, int num){
//
//        PreparedStatement ps = null;
//        try {
//            String sql = SELECT * FROM messages WHERE created_date BETWEEN '2020-01-01 00:00:00' AND '2020-06-05 19:00:00';
//
//            ps = connection.prepareStatement(sql);
//            ps.setString(1,"2020-01-01 00:00:00" );
//            ps.setString(2,"2020-06-05 18:00:00");
//
//
//            ResultSet rs = ps.executeQuery();
//            List<User> userList = toUserList(rs);
//            if (userList.isEmpty() == true) {
//                return null;
//            } else if (2 <= userList.size()) {
//                throw new IllegalStateException("2 <= userList.size()");
//            } else {
//                return userList.get(0);
//            }
//        } catch (SQLException e) {
//            throw new SQLRuntimeException(e);
//        } finally {
//            close(ps);
//        }
//
//    }
//}