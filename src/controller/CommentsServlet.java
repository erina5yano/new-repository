package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/Comments" })

public class CommentsServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	 @Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("./").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();


        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");
            System.out.println(user.getId());

            Comment comment = new Comment();
            comment.setText(request.getParameter("comment"));
            comment.setmessage_id(Integer.parseInt(request.getParameter("message_id")));
            comment.setUser_id(user.getId());
            new CommentService().register(comment);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

      String message = request.getParameter("comment");

        if (StringUtils.isBlank(message) == true) {
          messages.add("コメントを入力してください");
     }
        if (500 < message.length()) {
           messages.add("500文字以下で入力してください");        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
