<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"type="text/css" href="./css/top.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">

<div class="hedderlist">
<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
		<ul>
			<li><a href="newMessage">投稿する</a>
			<li><a href="usermg">ユーザー管理</a>
			<li><a href="logout">ログアウト</a>
		</ul>
</div>
		</div>

		<div class="messages">
	 <form action="./" method="get"><br />
		<label for="start">Start date:</label>
		<input type="date" id="start" name="start">

		<label for="end">End date:</label>
		<input type="date" id="end" name="end">

		<label>category:<input type="search" name="search" size="30" maxlength="255"></label>


		<input type="submit" value="検索" /> <br />
	 </form>

			<c:forEach items="${messages}" var="message">
			<div style ="padding: 10px; margin-bottom: 10px; border: 1px solid #333333; border-radius:10px;">
								<div class="title">
							件名：<c:out value="${message.title}" />
						</div>
						<span class="text">本文：<c:out value="${message.text}" /></span><br>
						<span class="category">カテゴリー：<c:out value="${message.category}" /></span><br>
						<span class="name">名前：<c:out value="${message.name}" /></span>
						<div class="date">
							<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

						<div class="message_delete">

							<c:if test="${loginUser.id == message.user_id}">
							<div>
								<form action="DeleteMessage" method="post">
									<input type="submit" value=" 削除 "/> (投稿の内容を削除します)<br>
									<input type="hidden" name="message_id" value="${message.id}"/>
								</form>
							</div>
							</c:if>
						</div>

				<form action="Comments" method="post">
					<textarea name="comment" cols="50" rows="5" id="text"></textarea><br />
					<input type="submit" value="コメントする" /> <br />
					<input type="hidden" name="message_id" value="${message.id}" />
				</form>
				<div class="comments">
				<c:forEach items="${comments}" var="comment">
					<c:if test="${message.id == comment.message_id}">
						<div class="comment">
							<div class="user_id-name">
								<span class="user_id"><c:out value="${comment.user_id}"/></span>
								<span class="name"><c:out value="${comment.name}"/></span>
							</div>
							<div class="text">
								<c:out value="${comment.text}" />
							</div>
							<div class="date">
								<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
						</div>
						<c:if test="${loginUser.id == comment.user_id}">
							<form action="DeleteComment" method="post">
								<input type="submit" value=" 削除 " /> (コメントの内容を削除します) <br>
								<input type="hidden" name="comment_id" value="${comment.id}" />
							</form>
						</c:if>
					</c:if>
				</c:forEach>
				</div>
				</div>
			</c:forEach>
		</div>
		</div>

	<div class="copyright">Copyright(c)YourName</div>
</body>
</html>