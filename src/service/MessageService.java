package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<UserMessage> getMessage(String start, String end,String search) {

		Date date = new Date();

		Connection connection = null;
	    try {
	        connection = getConnection();
	        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        UserMessageDao userMessageDao = new UserMessageDao();



	        if (!StringUtils.isEmpty(start)) {
				start = start + " 00:00:00";

			}else {
				//startの中身はnull
				start = "2020-01-01 00:00:00";
			}

	        if (!StringUtils.isEmpty(end)) {
	        	end = end + " 23:59:59";

			}else {

				end = simpleDateFormat.format(date);
			}

	        List<UserMessage> ret = userMessageDao.getUserMessages(connection,start,end,search);
	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}


public void delete(int message_id){

    Connection connection = null;
    try {
        connection = getConnection();

        MessageDao messageDao = new MessageDao();
        messageDao.delete(connection,  message_id);


        commit(connection);
    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}

}