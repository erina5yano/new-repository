<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"type="text/css" href="./css/top.css">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">

                <label for="account">アカウント</label> <input name="account"
                    id="account" /> <br /> <label for="password">パスワード</label> <input
                    name="password" type="password" id="password" /> <br /> <label
                    for="name">ユーザー名</label> <input name="name" id="name" /> <br />

                    <label for="branch_id">支社ID</label> <input name="branch_id"
                    id="branch_id" /> <br />
                    <label for="department_id">部署ID</label> <input name="department_id"
                    id="department_id" /> <br />

                <br /> <input type="submit" value="登録" /> <br /> <a href="./">トップへ</a>
            </form>
            <div class="copyright">Copyright(c)Your Name</div>
        </div>
    </body>
</html>