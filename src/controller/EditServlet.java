package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/edit" })

public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	int id =(Integer.parseInt(request.getParameter("id")));
    	UserService userService = new UserService();
    	User editUser = userService.edit(id);

    	request.setAttribute("user", editUser);
        request.getRequestDispatcher("edit.jsp").forward(request, response);

	 }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	int id =(Integer.parseInt(request.getParameter("id")));

    	User updateUser = new User();
    	updateUser.setAccount(request.getParameter("account"));
    	updateUser.setPassword(request.getParameter("password"));
    	updateUser.setName(request.getParameter("name"));
    	updateUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
    	updateUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

    	UserService userService = new UserService();
    	userService.update(id,updateUser);

        response.sendRedirect("usermg");

	 }
}